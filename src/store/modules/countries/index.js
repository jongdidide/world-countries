import actions from './actions.js'
import mutations from './mutations.js'
import getters from './getters.js'

const state = {
    countries: [],
    selectedCurrency: '',
}

const countries = {
    state,
    actions, 
    mutations, 
    getters, 
    namespaced: true
}

export default countries
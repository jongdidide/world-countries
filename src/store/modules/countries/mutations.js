const mutations = {
    SET_COUNTRIES(state, countries) {
        state.countries = countries
    },
    SELECTED_CURRENCY(state, currency) {
        state.selectedCurrency = currency
    }
}

export default mutations
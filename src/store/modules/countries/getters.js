const getters = {
    getFilteredCountries(state) {
        return state.countries.filter(({currencies}) => {
            return currencies.filter((currency) => {
                if (currency.name) {
                    return currency.name.includes(state.selectedCurrency)
                }
            }).length !== 0
        })
    },
    getPossibleCurrencies(state) {
        return new Set(
            state.countries.map((country) => {
                return country.currencies.map((currency) => {
                    if (currency.name !== '[D]' && currency.name !== '[E]' && currency.name !== ' ') {
                        return currency.name
                    }
                })
            }).flat(1)
        )
    }
}

export default getters
import Axios from "axios"

const actions = {
    async getAllCountries ({ commit }) {
     return Axios.get('https://restcountries.eu/rest/v2/all').then(response => {
         commit('SET_COUNTRIES', response.data)
     }).catch(error => {
         commit('GET_ERROR', error)
     })   
    },
    setSelectedCurrency({commit}, currency) {
        commit('SELECTED_CURRENCY', currency)
    }
}

export default actions
import Vuex from 'vuex'
import countries from './modules/countries/index'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        countries
    }
}) 
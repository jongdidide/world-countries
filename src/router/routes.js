import Main from '../views/Main.vue'
import Overview from '../views/Overview.vue'

const routes = [
    {
        path: '/',
        name: 'main',
        component: Main,
        children: [
            {
                path: '/overview',
                name: 'overview',
                component: Overview
            }
        ]

    }
]

export default routes